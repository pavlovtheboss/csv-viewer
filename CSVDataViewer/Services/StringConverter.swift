//
//  StringConverter.swift
//  CSVDataViewer
//
//  Created by Sergei Perevoznikov on 27/09/2017.
//  Copyright © 2017 Elements Interactive. All rights reserved.
//

import UIKit
import DTCoreText

extension String {
  func htmlAsAttributed() -> NSAttributedString {
    guard let data = data(using: .utf8) else {
      return NSAttributedString()
    }
    //let options: [String: Any] = [DTDefaultFontFamily: "Helvetica"]
    return DTHTMLAttributedStringBuilder(html: data, options: [:], documentAttributes: nil).generatedAttributedString()
  }
}
