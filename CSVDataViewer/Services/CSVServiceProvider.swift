//
//  CSVServiceProvider.swift
//  CSVDataViewer
//
//  Created by Sergei Perevoznikov on 27/09/2017.
//  Copyright © 2017 Elements Interactive. All rights reserved.
//

import UIKit
import Alamofire
import CSV.Swift

final class CSVServiceProvider: CSVService {
  private let queue = OperationQueue()
  
  init() {
    queue.name = "CSVServiceQueue"
  }
  
  func file(at url: URL, useCache: Bool, completion: @escaping (CSVFile?, CSVServiceError?) -> Void) {
    if useCache {
      openDownloadedFile(from: url, completion: completion)
    } else {
      downloadFile(at: url, completion: completion)
    }
  }
  
  private func openDownloadedFile(from url: URL, completion: @escaping (CSVFile?, CSVServiceError?) -> Void) {
    if let path = UserDefaults.standard.url(forKey: url.absoluteString) {
      queue.addOperation { [weak self] in
        do {
          let data = try Data(contentsOf: path)
          self?.parse(data: data, completion: completion)
        } catch {
          OperationQueue.main.addOperation { [weak self] in
            self?.downloadFile(at: url, completion: completion)
          }
        }
      }
    } else {
      downloadFile(at: url, completion: completion)
    }
  }
  
  private func downloadFile(at url: URL, completion: @escaping (CSVFile?, CSVServiceError?) -> Void) {
    let destination: DownloadRequest.DownloadFileDestination = { temporaryURL, response in
      let directoryURLs = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
      let options: DownloadRequest.DownloadOptions = [DownloadRequest.DownloadOptions.removePreviousFile]
      if !directoryURLs.isEmpty {
        return (directoryURLs[0].appendingPathComponent(response.suggestedFilename!), options)
      }
      
      return (temporaryURL, options)
    }
    
    Alamofire.download(url, to: destination).responseData { [weak self] response in
      if let error = response.result.error {
        completion(nil, CSVServiceError.downloadError(error.localizedDescription))
      }
      if let data = response.result.value {
        if let path = response.destinationURL {
          UserDefaults.standard.set(path, forKey: url.absoluteString)
          UserDefaults.standard.synchronize()
        }
        self?.parse(data: data, completion: completion)
      }
    }
  }
  
  private func parse(data: Data, completion: @escaping (CSVFile?, CSVServiceError?) -> Void) {
    queue.addOperation {
      guard let text = String(data: data, encoding: .utf8) else {
        completion(nil, CSVServiceError.badFileError(NSLocalizedString("encoding error", comment: "")))
        return
      }
      do {
        var rows: [CSVRow] = []
        let csv = try CSV(string: text, hasHeaderRow: true)
        while let row = csv.next() {
          rows.append(CSVRow(csvRow: row))
        }
        OperationQueue.main.addOperation {
          completion(CSVFile(rows: rows), nil)
        }
      } catch (let parseError) {
        OperationQueue.main.addOperation {
          completion(nil, CSVServiceError.badFileError(parseError.localizedDescription))
        }
      }
    }
  }
}

