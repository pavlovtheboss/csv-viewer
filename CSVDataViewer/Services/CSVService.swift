//
//  CSVService.swift
//  CSVDataViewer
//
//  Created by Sergei Perevoznikov on 27/09/2017.
//  Copyright © 2017 Elements Interactive. All rights reserved.
//

import UIKit

enum CSVServiceError: Error {
  case badFileError(String)
  case downloadError(String)
}

protocol CSVService {
  func file(at url: URL, useCache: Bool, completion: @escaping (CSVFile?, CSVServiceError?) -> Void)
}
