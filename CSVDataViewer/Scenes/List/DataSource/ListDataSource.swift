//
//  ListDataSource.swift
//  CSVDataViewer
//
//  Created by Sergei Perevoznikov on 27/09/2017.
//  Copyright © 2017 Elements Interactive. All rights reserved.
//

import UIKit

final class ListDataSource: NSObject, UITableViewDataSource {
  private let listItemCellReuseIdentifier = "ListItemCell"
  private let listItemCellNibName = "ListItemCell"
  var items: [List.Item] = []
  
  func item(at indexPath: IndexPath) -> List.Item? {
    return items[indexPath.row]
  }
  
  func register(_ tableView: UITableView) {
    let nib = UINib(nibName: listItemCellNibName, bundle: Bundle.main)
    tableView.register(nib, forCellReuseIdentifier: listItemCellReuseIdentifier)
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return items.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: listItemCellReuseIdentifier, for: indexPath)
    if let cell = cell as? ListItemCell,
      let item = item(at: indexPath) {
      cell.setup(item)
    }
    return cell
  }
}
