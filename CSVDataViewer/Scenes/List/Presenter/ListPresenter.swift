//
//  ListPresenter.swift
//  CSVDataViewer
//
//  Created by Sergei Perevoznikov on 27/09/2017.
//  Copyright © 2017 Elements Interactive. All rights reserved.
//

import UIKit

final class ListPresenter: ListPresentationLogic {
  weak var output: ListDisplayLogic?
  func present(_ response: List.Reload.Response) {
    DispatchQueue.global(qos: .utility).async {
      var items: [List.Item]? = nil
      if let rows = response.file?.rows {
        items = rows.flatMap { row in
          guard let title = row.title else { return nil }
          return List.Item(title: title, description: row.description, imageURL: row.imageURL)
        }
      }
      var error: String?
      
      if let responseError = response.error {
        switch responseError{
        case .downloadError(let description):
          error = "\(NSLocalizedString("Fail to download file", comment: "")) (\(description))"
        case .badFileError(let description):
          error = "\(NSLocalizedString("Bad file format", comment: "")) (\(description))"
        }
      }
      
      let viewModel = List.Reload.ViewModel(items: items, error: error)
      DispatchQueue.main.async { [weak self] in
        self?.output?.display(viewModel: viewModel)
      }
    }
  }
}
