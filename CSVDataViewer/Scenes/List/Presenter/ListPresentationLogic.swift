//
//  ListPresentationLogic.swift
//  CSVDataViewer
//
//  Created by Sergei Perevoznikov on 27/09/2017.
//  Copyright © 2017 Elements Interactive. All rights reserved.
//

import UIKit

protocol ListPresentationLogic {
  func present(_ response: List.Reload.Response)
}
