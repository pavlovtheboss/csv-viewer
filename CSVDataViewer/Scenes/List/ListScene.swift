//
//  ListScene.swift
//  CSVDataViewer
//
//  Created by Sergei Perevoznikov on 27/09/2017.
//  Copyright © 2017 Elements Interactive. All rights reserved.
//

import UIKit

struct List {
  struct Item {
    var title: String
    var imageURL: URL?
    var description: String?
    init(title: String, description: String?, imageURL: URL?) {
      self.title = title
      self.imageURL = imageURL
      self.description = description
    }
  }
  
  struct Reload {
    struct Request {
      var useCache: Bool
    }
    
    struct Response {
      var file: CSVFile?
      var error: CSVServiceError?
    }
    
    struct ViewModel {
      var items: [Item]?
      var error: String?
    }
  }
}
