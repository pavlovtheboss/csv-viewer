//
//  ListViewController.swift
//  CSVDataViewer
//
//  Created by Sergei Perevoznikov on 27/09/2017.
//  Copyright © 2017 Elements Interactive. All rights reserved.
//

import UIKit

final class ListViewController: UIViewController, ListDisplayLogic, UITableViewDelegate {
  var output: ListBusinessLogic?
  var router: ListRoutingLogic?
  private let dataSource = ListDataSource()
  
  @IBOutlet weak var tableView: UITableView!
  private weak var refreshControl: UIRefreshControl?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let refreshControl = UIRefreshControl()
    refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
    tableView.addSubview(refreshControl)
    self.refreshControl = refreshControl
    
    dataSource.register(tableView)
    tableView.dataSource = dataSource
    
    refreshControl.beginRefreshing()
    DispatchQueue.main.async {
      self.output?.process(List.Reload.Request(useCache: true))
    }
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    refreshControl?.endRefreshing()
  }
  
  @objc func refresh(_ sender: Any?) {
    output?.process(List.Reload.Request(useCache: false))
  }
  // MARK: - Navigation
  
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    router?.passDataToNextScene(segue: segue)
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    if let item = dataSource.item(at: indexPath) {
      router?.showDetail(item)
    }
  }
  
  //MARK: - Display Logic
  func display(viewModel: List.Reload.ViewModel) {
    if let items = viewModel.items {
      dataSource.items = items
      tableView.reloadData()
    }
    
    if let error = viewModel.error {
      displayError(message: error)
    }
    
    refreshControl?.endRefreshing()
  }
  
  private func displayError(message: String) {
    let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: nil))
    present(alert, animated: true, completion: nil)
  }
}
