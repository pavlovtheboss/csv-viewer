//
//  ListItemCell.swift
//  CSVDataViewer
//
//  Created by Sergei Perevoznikov on 27/09/2017.
//  Copyright © 2017 Elements Interactive. All rights reserved.
//

import UIKit
import DTCoreText
import AlamofireImage

fileprivate let filter = AspectScaledToFillSizeWithRoundedCornersFilter(size: CGSize(width: 40, height: 40), radius: 20.0)

final class ListItemCell: UITableViewCell {
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var descriptionLabel: DTAttributedLabel!
  @IBOutlet weak var previewImageView: UIImageView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
    descriptionLabel.numberOfLines = 2
    descriptionLabel.lineBreakMode = .byTruncatingTail
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    titleLabel.text = nil
    descriptionLabel.attributedString = nil
    previewImageView.image = nil
    descriptionLabel.isHidden = true
  }
  
  func setup(_ viewModel: List.Item) {
    titleLabel.text = viewModel.title
    
    if let imageURL = viewModel.imageURL {
      
      previewImageView.af_setImage(
        withURL: imageURL,
        placeholderImage: nil,
        filter: filter,
        imageTransition: .crossDissolve(0.2)
      )
    }
    
    if let description = viewModel.description {
      descriptionLabel.attributedString = description.htmlAsAttributed()
      descriptionLabel.isHidden = description.isEmpty
      descriptionLabel.sizeToFit()
    }
  }
}

