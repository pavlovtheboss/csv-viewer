//
//  ListInteractor.swift
//  CSVDataViewer
//
//  Created by Sergei Perevoznikov on 27/09/2017.
//  Copyright © 2017 Elements Interactive. All rights reserved.
//

import UIKit

final class ListInteractor: ListBusinessLogic {
  var output: ListPresentationLogic?
  private let csvFile = "https://docs.google.com/spreadsheet/ccc?key=0Aqg9JQbnOwBwdEZFN2JKeldGZGFzUWVrNDBsczZxLUE&single=true&gid=0&output=csv"
  
  private var service: CSVService
  init(service: CSVService) {
    self.service = service
  }
  
  func process(_ request: List.Reload.Request) {
    guard let url = URL(string: csvFile) else { return }
    service.file(at: url, useCache: request.useCache) { [weak self] file, error in
      let response = List.Reload.Response(file: file, error: error)
      self?.output?.present(response)
    }
  }
}
