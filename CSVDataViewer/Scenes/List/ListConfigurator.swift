//
//  ListConfigurator.swift
//  CSVDataViewer
//
//  Created by Sergei Perevoznikov on 27/09/2017.
//  Copyright © 2017 Elements Interactive. All rights reserved.
//

import UIKit

final class ListConfigurator {
  func configurate(_ viewController: ListViewController) {
    let interactor = ListInteractor(service: CSVServiceProvider())
    let router = ListRouter()
    let presenter = ListPresenter()
    viewController.output = interactor
    viewController.router = router
    
    interactor.output = presenter
    presenter.output = viewController
    
    router.viewController = viewController
  }
}
