//
//  ListRouter.swift
//  CSVDataViewer
//
//  Created by Sergei Perevoznikov on 27/09/2017.
//  Copyright © 2017 Elements Interactive. All rights reserved.
//

import UIKit

final class ListRouter: ListRoutingLogic {
  weak var viewController: UIViewController?
  private let detailIdentifier = "showDetail"
  private var detailConfigurator: DetailConfigurator?
  
  func passDataToNextScene(segue: UIStoryboardSegue) {
    if segue.identifier == detailIdentifier,
      let viewController = segue.destination as? DetailViewController {
      detailConfigurator?.configurate(viewController)
    }
  }
  
  func showDetail(_ model: List.Item) {
    detailConfigurator = DetailConfigurator(title: model.title, description: model.description, imageURL: model.imageURL)
    viewController?.performSegue(withIdentifier: detailIdentifier, sender: nil)
  }
}
