//
//  DetailBusinessLogic.swift
//  CSVDataViewer
//
//  Created by Sergei Perevoznikov on 27/09/2017.
//  Copyright © 2017 Elements Interactive. All rights reserved.
//

import UIKit

protocol DetailBusinessLogic {
  func process(_ request: Detail.Reload.Request)
  func process(_ request: Detail.DownloadImage.Request)
}
