//
//  DetailInteractor.swift
//  CSVDataViewer
//
//  Created by Sergei Perevoznikov on 27/09/2017.
//  Copyright © 2017 Elements Interactive. All rights reserved.
//

import UIKit
import AlamofireImage

final class DetailInteractor: DetailBusinessLogic {
  var output: DetailPresentationLogic?
  private var title: String
  private var description: String?
  private var imageURL: URL?
  
  init(title: String, description: String?, imageURL: URL?) {
    self.title = title
    self.description = description
    self.imageURL = imageURL
  }
  
  func process(_ request: Detail.Reload.Request) {
    output?.present(Detail.Reload.Response(title: title, description: description, imageURL: imageURL))
  }
  
  func process(_ request: Detail.DownloadImage.Request) {
    ImageDownloader.default.download(URLRequest(url: request.imageURL)) { [weak self] response in
      self?.output?.present(Detail.DownloadImage.Response(image: response.result.value, error: response.result.error))
    }
  }
}
