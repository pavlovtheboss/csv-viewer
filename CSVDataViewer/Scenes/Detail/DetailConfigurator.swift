//
//  DetailConfigurator.swift
//  CSVDataViewer
//
//  Created by Sergei Perevoznikov on 27/09/2017.
//  Copyright © 2017 Elements Interactive. All rights reserved.
//

import UIKit

final class DetailConfigurator {
  private var title: String
  private var description: String?
  private var imageURL: URL?
  
  init(title: String, description: String?, imageURL: URL?) {
    self.title = title
    self.description = description
    self.imageURL = imageURL
  }
  
  func configurate(_ viewController: DetailViewController) {
    let interactor = DetailInteractor(title: title, description: description, imageURL: imageURL)
    let router = DetailRouter()
    let presenter = DetailPresenter()
    viewController.output = interactor
    viewController.router = router
    
    interactor.output = presenter
    presenter.output = viewController
    
    router.viewController = viewController
  }
}

