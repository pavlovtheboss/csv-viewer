//
//  DetailScene.swift
//  CSVDataViewer
//
//  Created by Sergei Perevoznikov on 28/09/2017.
//  Copyright © 2017 Elements Interactive. All rights reserved.
//

import UIKit

struct Detail {
  struct Item {
    var title: String
    var description: String?
    var imageURL: URL?
    init(title: String, description: String?, imageURL: URL?) {
      self.title = title
      self.imageURL = imageURL
      self.description = description
    }
  }
  struct Reload {
    struct Request {
      
    }
    
    struct Response {
      var title: String
      var description: String?
      var imageURL: URL?
    }
    
    struct ViewModel {
      var item: Item
    }
  }
  struct DownloadImage {
    struct Request {
      var imageURL: URL
    }
    
    struct Response {
      var image: UIImage?
      var error: Error?
    }
    
    struct ViewModel {
      var image: UIImage
    }
  }
}
