//
//  DetailPresenter.swift
//  CSVDataViewer
//
//  Created by Sergei Perevoznikov on 27/09/2017.
//  Copyright © 2017 Elements Interactive. All rights reserved.
//

import UIKit

final class DetailPresenter: DetailPresentationLogic {
  weak var output: DetailDisplayLogic?
  func present(_ response: Detail.Reload.Response) {
    let item = Detail.Item(title: response.title, description: response.description, imageURL: response.imageURL)
    output?.display(viewModel: Detail.Reload.ViewModel(item: item))
  }
  
  func present(_ response: Detail.DownloadImage.Response) {
    if let image = response.image {
      output?.display(viewModel: Detail.DownloadImage.ViewModel(image: image))
    }
  }
}
