//
//  DetailPresentationLogic.swift
//  CSVDataViewer
//
//  Created by Sergei Perevoznikov on 27/09/2017.
//  Copyright © 2017 Elements Interactive. All rights reserved.
//

import UIKit

protocol DetailPresentationLogic {
  func present(_ response: Detail.Reload.Response)
  func present(_ response: Detail.DownloadImage.Response)
}
