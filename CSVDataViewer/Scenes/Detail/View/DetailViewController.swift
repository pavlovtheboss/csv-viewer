//
//  DetailViewController.swift
//  CSVDataViewer
//
//  Created by Sergei Perevoznikov on 27/09/2017.
//  Copyright © 2017 Elements Interactive. All rights reserved.
//

import UIKit
import DTCoreText

final class DetailViewController: UIViewController, DetailDisplayLogic {
  var output: DetailBusinessLogic?
  var router: DetailRoutingLogic?
  
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var descriptionLabel: DTAttributedLabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    titleLabel.numberOfLines = 0
    descriptionLabel.numberOfLines = 0
    imageView.isHidden = true
    // Do any additional setup after loading the view.
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    output?.process(Detail.Reload.Request())
  }
  
  func display(viewModel: Detail.Reload.ViewModel) {
    if let imageURL = viewModel.item.imageURL {
      output?.process(Detail.DownloadImage.Request(imageURL: imageURL))
    }
    
    titleLabel.text = viewModel.item.title
    titleLabel.sizeToFit()
    
    descriptionLabel.attributedString = viewModel.item.description?.htmlAsAttributed()
    descriptionLabel.sizeToFit()
  }
  
  func display(viewModel: Detail.DownloadImage.ViewModel) {
    imageView.isHidden = false
    imageView.image = viewModel.image
  }
}

