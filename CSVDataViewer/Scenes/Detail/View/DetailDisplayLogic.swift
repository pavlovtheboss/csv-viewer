//
//  DetailDisplayLogic.swift
//  CSVDataViewer
//
//  Created by Sergei Perevoznikov on 27/09/2017.
//  Copyright © 2017 Elements Interactive. All rights reserved.
//

import UIKit

protocol DetailDisplayLogic: class {
  func display(viewModel: Detail.Reload.ViewModel)
  func display(viewModel: Detail.DownloadImage.ViewModel)
}
