//
//  CSVRow.swift
//  CSVDataViewer
//
//  Created by Sergei Perevoznikov on 27/09/2017.
//  Copyright © 2017 Elements Interactive. All rights reserved.
//

import UIKit

final class CSVRow {
  var title: String?
  var imageURL: URL?
  var description: String?
  init(csvRow: [String]) {
    title = csvRow.count > 0 ? csvRow[0] : nil
    description = csvRow.count > 1 ? csvRow[1] : nil
    imageURL = csvRow.count > 2 ? URL(string: csvRow[2]) : nil
  }
}
